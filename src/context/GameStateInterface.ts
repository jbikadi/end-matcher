

export interface GameStateInterface {
    newGame: boolean
};

export const DEFAULT_STATE = {
    newGame: true
}