import {React, createContext, useContext} from 'react';

import {}

const createGlobalState = () => {
    return (
        <useContext.Provider value={}>

        </useContext.Provider>
    );
};

export const useGlobalState = () => {
    const context = useContext();
    return context;
};