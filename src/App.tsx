import React from 'react';
import Board from './components/Board'
import './scss/app.scss';
import { useGlobalContext } from './context/GlobalState';

function App() {
    const {state, setState} = useGlobalContext();

    return(
        <>
            <Board />
        </>
    );

}

export default App;