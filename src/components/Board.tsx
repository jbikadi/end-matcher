import Shape from './Shape';

function Board() {
    return (
        <div>
            <Shape />
        </div>
    );
}

export default Board;