

function Shape() {
    return(
        <>
            <div className="circle"><span className="visually-hidden">circle</span></div>
            <div className="triangle"><span className="visually-hidden">Triangle</span></div>
            <div className="square"><span className="visually-hidden">Square</span></div>
            <div className="diamond"><span className="visually-hidden">Diamond</span></div>
            <div className="moon"><span className="visually-hidden">Moon</span></div>
        </>
    );
}

export default Shape;